# Aplikasi Perbankan

Project ini bermaksud mereplika sistem perbankan dimana nasabah dapat melakukan transaksi yang berupa penarikan dan penyetoran, selain itu nasabah juga dapat melakukan pengajuan pinjaman dimana status persetujuan nya akan ditentukan oleh program dengan kualifikasi tertentu.

## Entitas Project

1. Nasabah
2. Tabungan
3. Transaksi
4. Pinjaman

## Database Table

![database](database.png)

Entitas nasabah memliki relation one-to-one dengan tabungan dan pinjaman sedangan tabungan memliki relation one-to-many dengan transaksi, karena nasabah hanya diperbolehkan memiliki satu pinjaman dan satu tabungan, sedangkan tabungan dapat memiliki banyak transaksi.

## Fungsi utama

### 1. Penyetoran

Untuk melakukan penyetoran dapat dilakukan pada transaksi resources contoh data JSON untuk penyetoran.

```
{
"tipe": "DEPOSIT",
"amount": 1000,
"keterangan": "Nabung januari"
}
```

Pastikan untuk membuat membuat nasabah dan tabungan terlebih dahulu, jika penyetoran berhasil maka saldo tabungan yang dimiliki nasabah akan otomatis bertambah.

### 2. Penarikan

Untuk melakukan penarikan dapat dilakukan pada transaksi resources contoh data JSON untuk penyetoran.

```
{
"tipe": "WITHDRAW",
"amount": 1000,
"keterangan": "Keperluan bootcamp"
}
```

Jika penarikan berhasil maka saldo tabungan yang dimiliki nasabah akan otomatis berkurang.

### 3. Pengajuan Pinjaman

Pengajuan pinjaman dapat dilakukan pada pinjaman resources, pinjaman hanya akan disetujui apabila memenuhi kualifikasi berikut:

- Usia nasabah kurang dari 45 tahun
- Jumlah pinjaman yang diajukan kurang dari 50% dari saldo tabungan nasabah
- Nasabah tidak memiliki pinjaman yang sebelumnya telah disetujui

Jika pinjaman ditolak maka data pinjaman tidak akan dimasukkan dalam database
