package com.tugasakhir.kelompok3;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.openapi.annotations.Operation;

import com.tugasakhir.kelompok3.entity.Tabungan;
import com.tugasakhir.kelompok3.entity.Transaksi;
import com.tugasakhir.kelompok3.entity.Transaksi.Status;
import com.tugasakhir.kelompok3.template.Template;

@Path("/transaksi")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TransaksiResources {

    @GET
    @Operation(summary = "Get semua data transaksi")
    public Response getAllTransaksi() {

        //Cek data
        if (Transaksi.count() == 0) {
            return gagal("Belum ada transaksi");
        }
        return berhasil("Berhasil", Transaksi.listAll());
    }

    @GET
    @Operation(summary = "Get transaksi by tabunganId, pastikan id tabungan valid")
    @Path("/{tabunganId}")
    public Response getTransaksiByTabunganId(@PathParam("tabunganId") Long tabunganId) {
        //Cek tabungan
        if (Tabungan.findById(tabunganId) == null) {
            return gagal("Tabungan tidak ditemukan");
        }
        List<Transaksi> transaksiTabungan = Transaksi.find("tabungan_id = ?1", tabunganId).list();
        //Cek transaksi
        if (transaksiTabungan.isEmpty()) {
            return gagal("Belum ada transaksi");
        }
        return Response.ok(new Template(true, "Berhasil", transaksiTabungan))
            .status(Response.Status.OK).build();
    }

    //Transaksi withdrawal dan deposit
    @POST
    @Operation(summary = "Buat transaksi baru, pastikan id tabungan valid", description = "Masukkan WITHDRAW pada kolom tipe jika ingin melakukan penarikan dan DEPOSIT jika ingin melakukan penyetoran, transaksi gagal tetap disimpan dalam database")
    @Path("/{tabunganId}")
    @Transactional
    public Response addTransaksi(@PathParam("tabunganId") Long tabunganId, @Valid Transaksi transaksi) {
        Tabungan tabungan = Tabungan.findById(tabunganId);

        //Jika tabungan tidak ditemukan
        if (tabungan == null) {
            return gagal("Transaksi tidak ditemukan");
        }
        tabungan.transaksiTabungan.add(transaksi);
        transaksi.persist();

        switch(transaksi.tipe) {

            //Withdrawal
            case WITHDRAW:
            if ((tabungan.saldoAkhir - transaksi.amount) < 0) {
                return gagal("Transaksi gagal - saldo tidak mencukupi");
            }
            tabungan.saldoAkhir = tabungan.saldoAkhir - transaksi.amount;
            transaksi.status = Status.BERHASIL;
            return Response.ok(new Template(true, "Berhasil", tabungan))
            .status(Response.Status.CREATED).build();

            //Deposit
            case DEPOSIT:
            tabungan.saldoAkhir = tabungan.saldoAkhir + transaksi.amount;
            transaksi.status = Status.BERHASIL;
            return Response.ok(new Template(true, "Berhasil", tabungan))
            .status(Response.Status.CREATED).build();
            
            default:
            return gagal("Transaksi gagal");
            }
        
    }

    @PUT
    @Operation(summary = "Update transaksi by id, hanya kolom keterangan yang bisa diupdate")
    @Transactional
    @Path("/{transaksiId}/update")
    public Object updateTransaksi(@PathParam("transaksiId") Long transaksiId, @Valid Transaksi newTransaksi) {
        Transaksi transaksi = Transaksi.findById(transaksiId);

        if (transaksi == null) {
            return gagal("Transaksi tidak ditemukan");
        }

        if (newTransaksi.amount != null || newTransaksi.tipe != null) {
            return gagal("Hanya keterangan yang boleh di-update");
        }

        transaksi.keterangan = newTransaksi.keterangan;

        return berhasil("Berhasil", transaksi);

    }

    @DELETE
    @Operation(summary = "Delete transaksi by id")
    @Transactional
    @Path("{transaksiId}/delete")
    public Response deleteTransaksiById(@PathParam("transaksiId") Long transaksiId) {
        if (!(Transaksi.deleteById(transaksiId))) {
            return gagal("Transaksi tidak ditemukan");
        }
        return berhasil("Berhasil dihapus", null);
    }

     //Respon gagal
    private Response gagal(String message) {
        return Response.ok(new Template(false, message))
            .status(Response.Status.BAD_REQUEST).build();
    }

    //Respon berhasil
    private Response berhasil(String message, Object obj) {
        return Response.ok(new Template(true, message, obj))
            .status(Response.Status.OK).build();
    }
    
}
