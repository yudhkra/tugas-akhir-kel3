package com.tugasakhir.kelompok3.entity;

import java.time.Instant;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Min;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
public class Transaksi extends PanacheEntityBase {

    public enum Tipe{WITHDRAW, DEPOSIT};
    public enum Status{GAGAL, BERHASIL};
    @Id
    @SequenceGenerator(
        name = "transaksiSequence",
        sequenceName = "transaksi_id_seq",
        allocationSize = 1,
        initialValue = 1
    )
    @GeneratedValue(
        strategy = GenerationType.SEQUENCE,
        generator = "transaksiSequence"
    )
    @Schema(readOnly = true)
    public Long id;
    public Tipe tipe;
    @Schema(readOnly = true)
    public Instant timeStamp = Instant.now();
    @Min(value = 100, message = "Jumlah transaksi minimal 100")
    public Double amount;

    public String keterangan;

    @Schema(readOnly = true)
    public Status status = Status.GAGAL;

    public Transaksi() {
    }

    
}
