package com.tugasakhir.kelompok3.entity;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotBlank;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import com.fasterxml.jackson.annotation.JsonBackReference;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
public class Tabungan extends PanacheEntityBase {

    @Id
    @SequenceGenerator(
        name = "tabunganSequence",
        sequenceName = "tabungan_id_seq",
        allocationSize = 1,
        initialValue = 1
    )
    @GeneratedValue(
        strategy = GenerationType.SEQUENCE,
        generator = "tabunganSequence"
    )
    @Schema(readOnly = true)
    public Long id;

    @NotBlank(message = "Tidak boleh kosong")
    public String nama;

    @NotBlank(message = "Tidak boleh kosong")
    public String kode;

    @NotBlank(message = "Tidak boleh kosong")
    public String cabang;

    @Schema(readOnly = true)
    private static Instant instant = Instant.now();

    @Column(name = "tanggal_pembuatan")
    @Schema(readOnly = true)
    public LocalDate tanggalPembuatan = LocalDate.now();

    @Schema(readOnly = true)
    @Column(name = "saldo_akhir")
    public Double saldoAkhir = 0.0;

    @OneToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "nasabah_id")
    @JsonBackReference
    @Schema(readOnly = true)
    public Nasabah nasabah;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "tabungan_id", referencedColumnName = "id")
    @Schema(readOnly = true)
    public List<Transaksi> transaksiTabungan;

    public Tabungan() {
    }


}
