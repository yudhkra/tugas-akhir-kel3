package com.tugasakhir.kelompok3.entity;

import java.time.Instant;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import com.fasterxml.jackson.annotation.JsonBackReference;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
public class Pinjaman extends PanacheEntityBase {
    
    @Id
    @SequenceGenerator(
        name = "pinjamanSequence",
        sequenceName = "pinjaman_id_seq",
        allocationSize = 1,
        initialValue = 1
    )
    @GeneratedValue(
        strategy = GenerationType.SEQUENCE,
        generator = "pinjamanSequence"
    )
    @Schema(readOnly = true)
    public Long id;
    
    @Column(name = "waktu_pengajuan")
    @Schema(readOnly = true)
    public Instant waktuPengajuan = Instant.now();

    @Min(value = 100)
    public Double amount;

    @Column(name = "tujuan_pinjaman")
    @NotBlank(message = "Tidak boleh kosong")
    public String tujuanPinjaman;

    @Column(name = "status_acc")
    @Schema(readOnly = true)
    public Boolean statusAcc;

    @OneToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "nasabah_id")
    @JsonBackReference
    @Schema(readOnly = true)
    public Nasabah nasabah;

    public Pinjaman() {
    }


}
