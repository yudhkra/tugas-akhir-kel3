package com.tugasakhir.kelompok3.entity;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import javax.validation.constraints.NotNull;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
public class Nasabah extends PanacheEntityBase {

    @Id
    @SequenceGenerator(
        name = "nasabahSequence",
        sequenceName = "nasabah_id_seq",
        allocationSize = 1,
        initialValue = 1
    )
    @GeneratedValue(
        strategy = GenerationType.SEQUENCE,
        generator = "nasabahSequence"
    )
    @Schema(readOnly = true)
    public Long id;

    @NotBlank(message = "Tidak boleh kosong")
    public String nama;

    @NotBlank(message = "Tidak boleh kosong")
    public String alamat;

    @Column(name = "tanggal_lahir")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Past(message = "Tidak valid")
    @NotNull(message = "Tidak boleh kosong")
    public LocalDate tanggalLahir;

    @Schema(readOnly = true)
    public Integer usia;

    @OneToOne(mappedBy = "nasabah", cascade = CascadeType.ALL)
    @JsonManagedReference
    @Schema(readOnly = true)
    public Tabungan tabunganNasabah;

    @OneToOne(mappedBy = "nasabah", cascade = CascadeType.ALL)
    @JsonManagedReference
    @Schema(readOnly = true)
    public Pinjaman pinjamanNasabah;

    public Nasabah() {
    }

    
}
