package com.tugasakhir.kelompok3;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.openapi.annotations.Operation;

import com.tugasakhir.kelompok3.entity.Nasabah;
import com.tugasakhir.kelompok3.entity.Tabungan;
import com.tugasakhir.kelompok3.template.Template;

@Path("/tabungan")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TabunganResources {

    @GET
    @Operation(summary = "Get semua data tabungan")
    public Response getTabungan() {

        //Jika belum ada data 
        if (Tabungan.count() == 0) {
            return gagal("Tabungan tidak ditemukan");
        } 
        return berhasil(Tabungan.listAll());
    }

    @GET
    @Operation(summary = "Get data tabungan by tabunganId")
    @Path("{tabunganId}")
    public Response getTabunganById(@PathParam("tabunganId") Long tabunganId) {
        Tabungan tabungan = Tabungan.findById(tabunganId);

        //Jika data tidak ditemukan
        if (tabungan == null) {
            return gagal("Tabungan tidak ditemukan");
        }
        return berhasil(tabungan);
    }

    @POST
    @Operation(summary = "Tambahkan tabungan baru, pastikan nasabah id tersedia", description = "Data hanya akan disimpan dalam database jika nasabah belum memiliki tabungan")
    @Path("{nasabahId}/add")
    @Transactional
    public Response addTabungan(@PathParam("nasabahId") Long nasabahId, @Valid Tabungan tabungan) {
        Nasabah nasabah = Nasabah.findById(nasabahId);

        //Jika nasabah tidak ditemukan
        if (nasabah == null) {
            return Response.ok(new Template(false, "Nasabah tidak ditemukan"))
            .status(Response.Status.NOT_FOUND).build();
        }

        //Jika nasabah sudah memiliki tabungan
        if (nasabah.tabunganNasabah != null) {
            return gagal("Nasabah sudah memiliki tabungan");
        }

        tabungan.nasabah = nasabah;
        tabungan.persist();
        return berhasil(tabungan);
    }

    @PUT
    @Operation(summary = "Update data tabungan by id")
    @Path("{tabunganId}")
    @Transactional
    public Response updateTabungan(@PathParam("tabunganId") Long id, @Valid Tabungan newTabungan) {
        Tabungan tabungan = Tabungan.findById(id);

        //Jika tabungan tidak ditemukan
        if (tabungan == null) {
            return gagal("Tabungan tidak ditemukan");
        }

        tabungan.nama = newTabungan.nama;
        tabungan.kode = newTabungan.kode;

        return berhasil(tabungan);
    }

    @DELETE
    @Operation(summary = "Delete tabungan by id")
    @Path("{tabunganId}/delete")
    @Transactional
    public Response deleteTabungan(@PathParam("tabunganId") Long tabunganId) {

        //Jika tabungan tidak ditemukan
        if (!(Tabungan.deleteById(tabunganId))) {
            return gagal("Tabungan tidak ditemukan");
        }
        return berhasil(null);
    }

    //Tabungan tidak ditemukan respon
    private Response gagal(String message) {
        return Response.ok(new Template(false, message))
            .status(Response.Status.NOT_FOUND).build();
    }

    //Respon berhasil
    private Response berhasil(Object obj) {
        return Response.ok(new Template(true, "Berhasil", obj))
        .status(Response.Status.OK).build();
    }

}