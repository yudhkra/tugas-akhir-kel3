package com.tugasakhir.kelompok3;

import java.time.LocalDate;
import java.time.Period;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.openapi.annotations.Operation;

import com.tugasakhir.kelompok3.entity.Nasabah;
import com.tugasakhir.kelompok3.template.Template;

@Path("/nasabah")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class NasabahResources {

    @GET
    @Operation(summary = "Get semua data nasabah")
    public Response getNasabah() {

        //Jika tidak ada data nasabah
        if (Nasabah.count() == 0) {
            return notFound();
        } 

        return berhasil(Nasabah.listAll());
    }

    @GET
    @Operation(summary = "Get data nasabah by id")
    @Path("/{id}")
    public Response getNasabahById(@PathParam("id") Long id) {
        Nasabah nasabah = Nasabah.findById(id);

        //Jika tidak ada data nasabah
        if (nasabah == null) {
            return notFound();
        }

        return berhasil(nasabah);
    }

    @POST
    @Operation(summary = "Tambahkan nasabah baru, usia akan tergenerate secara otomatis")
    @Path("/add")
    @Transactional
    public Object addNasabah(@Valid Nasabah nasabah) {
        nasabah.usia = hitungUsia(nasabah.tanggalLahir);
        nasabah.persist();
        return berhasil(nasabah);
    }

    @PUT
    @Operation(summary = "Update data nasabah by id")
    @Transactional
    @Path("{id}/update")
    public Response nasabahUpdate(@PathParam("id") Long id, @Valid Nasabah newNasabah) {
        Nasabah nasabah = Nasabah.findById(id);

        //Jika tidak ada data nasabah
        if (nasabah == null) {
            return notFound();
        }

        nasabah.nama = newNasabah.nama;
        nasabah.alamat = newNasabah.alamat;
        nasabah.tanggalLahir = newNasabah.tanggalLahir;
        nasabah.usia = hitungUsia(nasabah.tanggalLahir);

        return berhasil(nasabah);
    }
    
    @DELETE
    @Operation(summary = "Delete data nasabah by id")
    @Transactional
    @Path("{id}/delete")
    public Response deleteNasabah(@PathParam("id") Long id) {
        if (!(Nasabah.deleteById(id))) {
            return notFound();
        }
        return berhasil(null);
    }

    //Nasabah tidak ditemukan respon
    private Response notFound() {
        return Response.ok(new Template(false, "Nasabah tidak ditemukan"))
            .status(Response.Status.NOT_FOUND).build();
    }

    //Respon berhasil
    private Response berhasil(Object obj) {
        return Response.ok(new Template(true, "Berhasil", obj))
        .status(Response.Status.OK).build();
    }

    //Hitung usia nasabah
    private Integer hitungUsia(LocalDate dob) {
        LocalDate now = LocalDate.now();
        return Period.between(dob, now).getYears();
    }
    
}
