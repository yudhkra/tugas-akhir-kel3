package com.tugasakhir.kelompok3;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.openapi.annotations.Operation;

import com.tugasakhir.kelompok3.entity.Nasabah;
import com.tugasakhir.kelompok3.entity.Pinjaman;
import com.tugasakhir.kelompok3.entity.Tabungan;
import com.tugasakhir.kelompok3.template.Template;

@Path("/pinjaman")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PinjamanResources {

    @GET
    @Operation(summary = "Get semua data pinjaman")
    public Response getPinjaman() {
        if (Pinjaman.count() == 0) {
            return gagal("Pinjaman tidak ditemukan - masukkan data terlebih dahulu");
        }
        return berhasil("Berhasil", Pinjaman.listAll());   
    }

    @GET
    @Operation(summary = "Get data pinjaman by id")
    @Path("/{pinjamanId}")
    public Response getPinjamanById(@PathParam("pinjamanId") Long pinjamanId) {
        Pinjaman pinjaman = Pinjaman.findById(pinjamanId);
        if (pinjaman == null) {
            return gagal("Pinjaman tidak ditemukan");
        }
        return berhasil("Berhasil", pinjaman);
    }

    @POST
    @Operation(summary = "Buat pinjaman baru", 
    description = "Pinjaman akan disetujui apabila nasabah memiliki usia kurang dari 45 tahun dan jumlah pinjaman kurang dari 50% dari jumlah saldo pada tabungan dan nasabah tidak memliki pinjaman yang telah disetujui sebelumnya, dan data pinjaman hanya akan disimpan dalam database apabila disetujui")
    @Path("/{nasabahId}")
    @Transactional
    public Response daftarPinjaman(@PathParam("nasabahId") Long nasabahId, @Valid Pinjaman pinjaman) {
        Nasabah nasabah = Nasabah.findById(nasabahId);

        //Pengecekan nasabah
        if (nasabah == null) {
            return gagal("Nasabah tidak ditemukan");
        }

        Tabungan tabunganNasabah = nasabah.tabunganNasabah;
        //Pengecekan tabungan nasabah
        if (tabunganNasabah == null) {
            return gagal("Nasabah belum memiliki tabungan - buat terlebih dahulu");
        }
        
        //Pinjaman akan ditolak jika nasabah sudah memiliki pinjaman sebelumnya yang telah di-acc
        //dan data tidak akan disimpan
        if (nasabah.pinjamanNasabah != null && nasabah.pinjamanNasabah.statusAcc == true) {
                return gagal("Pinjaman ditolak - anda telah memiliki pinjaman sebelumnya yang telah disetujui");
        }

        //Kualifikasi
        Boolean isAcc = kualifikasi(nasabah.usia, tabunganNasabah.saldoAkhir, pinjaman.amount);
        pinjaman.statusAcc = isAcc;
        pinjaman.nasabah = nasabah;

        //Data pinjaman hanya akan disimpan dalam DB jika status nya disetujui
        if (isAcc) {
            tabunganNasabah.saldoAkhir = tabunganNasabah.saldoAkhir + pinjaman.amount;
            pinjaman.persist();
            return berhasil("Pinjaman disetujui saldo telah ditambahkan kedalam tabungan", pinjaman);
        } else {
            return gagal("Pinjaman ditolak - coba lagi dilain waktu");
        }
    }

    @PUT
    @Path("{pinjamanId}/update")
    @Operation(summary = "Update data pinjaman", description = "Hanya kolom tujuanPinjaman yang boleh diupdate")
    @Transactional
    public Response updatePinjaman(@PathParam("pinjamanId") Long pinjamanId, @Valid Pinjaman newPinjaman) {
        Pinjaman pinjaman = Pinjaman.findById(pinjamanId);
        if (pinjaman == null) {
            return gagal("Pinjaman tidak ditemukan");
        }
        if (newPinjaman.amount != null) return gagal("Hanya tujuan pinjaman yang boleh di-update");
        pinjaman.tujuanPinjaman = newPinjaman.tujuanPinjaman;
        return berhasil("Berhasil", pinjaman);
    }

    @DELETE
    @Operation(summary = "Delete pinjaman by id")
    @Transactional
    @Path("{pinjamanId}/delete")
    public Response deletePinjamanById(@PathParam("pinjamanId") Long pinjamanId) {
        if (!(Pinjaman.deleteById(pinjamanId))) {
            return gagal("Pinjaman tidak ditemukan");
        }
        return berhasil("Berhasil dihapus", null);
    }

    private Response berhasil(String message, Object obj) {
        return Response.ok(new Template(true, message, obj))
        .status(Response.Status.OK).build();
    }

    private Response gagal(String message) {
        return Response.ok(new Template(false, message))
        .status(Response.Status.OK).build();
    }

    /**
     * Kualifikasi function
     * Pinjaman hanya akan disetujui apabila usia nasabah kurang dari 45 tahun dan jumlah pinjaman
     * tidak lebih dari setengah dari saldo nasabah
     */
    private Boolean kualifikasi(Integer usia, Double saldoAkhir, Double jumlahPinjaman) {
        if (usia < 45) {
            if ((saldoAkhir / 2) > jumlahPinjaman) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
        
    }
    
}
